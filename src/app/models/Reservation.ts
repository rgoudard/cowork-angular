export class Reservation {
  id: number;
  startDate: Date;
  endDate: Date;
  idMaterial: number;
  idUser: number;
}
