export class Site {
  id: number;
  name: string;
  address: string;
  hightWiFi: boolean;
  trays: boolean;
  illimitedCons: boolean;

}
