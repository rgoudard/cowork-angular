export class Ticket {
  id: number;
  title: string;
  subject: string;
  idUser: number;
  idUserDev: number;
  status: 'EN ATTENTE D\'AFFECTION' | 'AFFECTER' | 'EN ATTENTE DE REPONSE UTILISATEUR' | 'EN ATTENTE DE REPONSE DEVELOPPEUR' |'TERMINER';
  // tslint:disable-next-line:variable-name
  created_at: Date;
  type: string;
  idMaterial: number;
}
