export class SubscriptionType {
  id: number;
  name: string;
  description: string;
  hourPrice: number;
  dayPrice: number;
  studentPrice: number;
  subscriptionPrice: number;
  agreementPrice: number;
  agreementTime: number;
}
