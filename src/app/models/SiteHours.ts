export class SiteHours {
  id: number;
  day: string;
  open: string;
  close: string;
  idSite: number;
}
