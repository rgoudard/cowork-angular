export class Material {
  id: number;
  type: string;
  name: string;
  reservable: boolean;
  idSite: number;
  defect: boolean;
}
