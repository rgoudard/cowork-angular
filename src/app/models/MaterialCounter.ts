export class MaterialCounter {
  idSite: number;
  countSR: number;
  countSA: number;
  countSalon: number;
  countPrinter: number;
  countComputer: number;
}
