import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { SigninComponent} from './components/signin/signin.component';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule} from '@angular/common/http';
import { ToastrModule} from 'ngx-toastr';
import { HomeComponent } from './components/home/home.component';
import { SignupComponent } from './components/signup/signup.component';
import { AccountComponent } from './components/account/account.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { OrderComponent } from './components/order/order.component';
import { MyTicketsComponent } from './components/my-tickets/my-tickets.component';
import {TicketComponent} from './components/my-tickets/ticket/ticket.component';
import { UsersComponent } from './components/users/users.component';
import { SitesComponent } from './components/sites/sites.component';
import {DatePipe, registerLocaleData} from '@angular/common';
import { SiteComponent } from './components/sites/site/site.component';
import { TicketsComponent } from './components/tickets/tickets.component';
import { OursitesComponent } from './components/oursites/oursites.component';
import { ViewReservationComponent } from './components/view-reservation/view-reservation.component';
import { ViewOrderComponent } from './components/view-order/view-order.component';
import { ViewTicketComponent } from './components/view-ticket/view-ticket.component';
import { ViewMaterialComponent } from './components/view-material/view-material.component';
import localeFr from '@angular/common/locales/fr';
import { NgxPayPalModule } from 'ngx-paypal';

registerLocaleData(localeFr);


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgxPayPalModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    SigninComponent,
    HomeComponent,
    SignupComponent,
    AccountComponent,
    ReservationComponent,
    SubscriptionComponent,
    OrderComponent,
    MyTicketsComponent,
    TicketComponent,
    UsersComponent,
    SitesComponent,
    SiteComponent,
    TicketsComponent,
    OursitesComponent,
    ViewReservationComponent,
    ViewOrderComponent,
    ViewTicketComponent,
    ViewMaterialComponent
  ],
  providers: [
    DatePipe,
    {provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
