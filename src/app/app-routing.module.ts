import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SigninComponent} from './components/signin/signin.component';
import {SignupComponent} from './components/signup/signup.component';
import {HomeComponent} from './components/home/home.component';
import {AccountComponent} from './components/account/account.component';
import {ReservationComponent} from './components/reservation/reservation.component';
import {OrderComponent} from './components/order/order.component';
import {SubscriptionComponent} from './components/subscription/subscription.component';
import {UsersComponent} from './components/users/users.component';
import {SitesComponent} from './components/sites/sites.component';
import {MyTicketsComponent} from './components/my-tickets/my-tickets.component';
import {TicketComponent} from './components/my-tickets/ticket/ticket.component';
import {SiteComponent} from './components/sites/site/site.component';
import {TicketsComponent} from './components/tickets/tickets.component';
import {OursitesComponent} from './components/oursites/oursites.component';
import {ViewReservationComponent} from './components/view-reservation/view-reservation.component';
import {ViewOrderComponent} from './components/view-order/view-order.component';
import {ViewTicketComponent} from './components/view-ticket/view-ticket.component';
import {ViewMaterialComponent} from './components/view-material/view-material.component';

const routes: Routes = [
  { path: '' , component: SigninComponent},
  { path: 'signup' , component: SignupComponent},
  { path: 'home' , component: HomeComponent},
  { path: 'account' , component: AccountComponent},
  { path: 'reservation' , component: ReservationComponent},
  { path: 'order' , component: OrderComponent},
  { path: 'sites' , component: SitesComponent},
  { path: 'sites/:siteId', component: SiteComponent},
  { path: 'users' , component: UsersComponent},
  { path: 'mytickets' , component: MyTicketsComponent},
  { path: 'ticket/:ticketId', component: TicketComponent},
  { path: 'ticket', component: TicketsComponent},
  { path: 'subscription' , component: SubscriptionComponent},
  { path: 'oursites' , component: OursitesComponent},
  { path: 'view-reservation', component: ViewReservationComponent },
  { path: 'view-order', component: ViewOrderComponent },
  { path: 'view-ticket', component: ViewTicketComponent },
  { path: 'view-material', component: ViewMaterialComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
