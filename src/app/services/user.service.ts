import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/User';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Md5} from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'http://51.91.10.171:3000';
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;



  constructor(private httpClient: HttpClient, private toastr: ToastrService, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  signIn(email: string, password: string) {
    const hashedPassword = Md5.hashStr(password);
    return this.httpClient.post(this.url + '/connection', {
      email,
      password: hashedPassword
    }).subscribe((response: { success: boolean, user: User, err: string }) => {
      if (response.success) {
        console.log(response.user);
        localStorage.setItem('user', JSON.stringify(response.user));
        this.currentUserSubject.next(response.user);
        this.toastr.success('Connexion réussie', 'SUCCES');
        return this.router.navigate(['/home']);
      } else {
        this.toastr.error('Mauvais nom de compte ou mot de passe', 'Erreur');
      }
    });
  }

  signOut() {
    localStorage.removeItem('user');
    return this.router.navigate(['/']);
  }

  createUser(lastname: string, firstname: string, email: string, password: string, repeatPassword: string) {
    const emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    if (lastname && firstname) {
      if (email) {
        if (email.match(emailPattern)) {
          if (password.length >= 8 && password.length <= 50) {
            if (password === repeatPassword) {
              const hashedPassword = Md5.hashStr(password);
              return this.httpClient.post(this.url + '/users', {
                lastname,
                firstname,
                email,
                password: hashedPassword
              }).subscribe((response: { success: boolean, user: number }) => {
                if (response.success) {
                  this.toastr.success('Création de compte réussie', 'SUCCES');
                  return this.httpClient.get(this.url + '/users/' + response.user)
                    .subscribe((userResponse: { user: User, success: boolean }) => {
                      if (userResponse.success) {
                        localStorage.setItem('user', JSON.stringify(userResponse.user));
                        this.currentUserSubject.next(userResponse.user);
                        return this.router.navigate(['/home']);
                      }
                    });
                } else {
                  this.toastr.error('Formulaire invalide', 'Erreur');
                }
              });
            } else {
              this.toastr.error('Vos mots de passe ne sont pas identiques', 'Erreur');
            }
          } else {
            this.toastr.error('Votre mot de passe ne respècte pas les règles de longueur', 'Erreur');
          }
        } else {
          this.toastr.error('Adresse email invalide', 'Erreur');
        }
      } else {
        this.toastr.error('Veuillez saisir un email', 'Erreur');
      }
    } else {
      this.toastr.error('Nom et Prénom obligatoires', 'Erreur');
    }
  }

  getUser(userId) {
    return this.httpClient.get(this.url + '/users/' + userId
    ).subscribe((response: { success: boolean, user: User, err: string }) => {
      if (response.success) {
        localStorage.setItem('user', JSON.stringify(response.user));
        this.currentUserSubject.next(response.user);
      } else {
        this.toastr.error('Utilisateur introuvable', 'Erreur');
      }
    });
  }

  updateInfoUser(lastname: string, firstname: string, email: string, password: string, userId: number) {
    const emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    if (lastname && firstname) {
      if (email) {
        if (email.match(emailPattern)) {
          return this.httpClient.put(this.url + '/users/' + userId, {
            lastname,
            firstname,
            email,
            password
          }).subscribe((response: { success: boolean, user: User, err: string }) => {
            if (response.success) {
              this.getUser(userId);
              this.toastr.success('Modification réussie', 'SUCCES');
            } else {
              this.toastr.error('Formulaire invalide', 'Erreur');
            }
          });
        } else {
          this.toastr.error('Adresse email invalide', 'Erreur');
        }
      } else {
        this.toastr.error('Veuillez saisir un email', 'Erreur');
      }
    } else {
      this.toastr.error('Nom et Prénom obligatoires', 'Erreur');
    }
  }

  updatePassword(userId: number, password: string) {
    const hashedPassword = Md5.hashStr(password);
    this.httpClient.put(this.url + '/user/' + userId, {
      password: hashedPassword
    }).subscribe((response: {success: boolean, err: string}) => {
        if (response.success) {
          this.getUser(userId);
          this.toastr.success('Modification réussie', 'SUCCES');
        } else {
          this.toastr.error('Formulaire invalide', 'Erreur');
        }
      });
  }

  removeUser(userId: number) {
    this.httpClient.delete(this.url + '/users/' + userId)
      .subscribe((response: {success: boolean ; err: string}) => {
        if (response.success) {
          this.toastr.success('Utilisateur supprimé avec succès', 'SUCCES');
          this.signOut();
        } else {
          this.toastr.error('Utilisateur non supprimé', 'Erreur');
        }
      });
  }
}
