import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    //
  }

  ngOnInit() {
  }

  onSubmit(email, password) {
    this.userService.signIn(email, password);
  }
}
