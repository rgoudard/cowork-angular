import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Ticket} from '../../models/Ticket';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';
import {Material} from '../../models/Material';

@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html'
})
export class ViewTicketComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private tickets: Ticket[];
  private users: User[];
  private filtred = false;
  private ticketFiltred: Array<Ticket> = [];
  private materials: Material[];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/tickets')
      .subscribe((response: {ticket: Ticket[]}) => {
        if (response.ticket.length > 0 ) {
          this.tickets = response.ticket;
        } else {
          console.log('Erreur de récupération des commandes');
        }
      });

    this.httpClient.get(this.url + '/users')
      .subscribe((response: {users: User[]}) => {
        if (response.users.length > 0 ) {
          this.users = response.users;
        } else {
          console.log('Erreur de récupération des utilisateurs');
        }
      });

    this.httpClient.get(this.url + '/material')
      .subscribe((response: { material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.materials = response.material;
        } else {
          console.log('Erreur de récupération des matériaux');
        }
      });
  }

  getUserName(idUser) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.users) {
      if (this.users[i].id === idUser) {
        str += this.users[i].firstName + ' ' + this.users[i].lastName;
      }
    }
    return str;
  }

  activeFilter(idUser, status, date) {
    if (idUser || status || date) {
      this.filtred = true;
      this.ticketFiltred = [];

      if (idUser && status && date) {
        idUser = toNumbers(idUser);

        for (const ticket of this.tickets) {
          if ((ticket.idUser === idUser[0] || ticket.idUserDev === idUser[0]) &&
            ticket.status === status &&
            new Date (ticket.created_at).toISOString().substr(0, 10) === date) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (idUser && status && !date) {
        idUser = toNumbers(idUser);

        for (const ticket of this.tickets) {
          if ((ticket.idUser === idUser[0] || ticket.idUserDev === idUser[0]) &&
            ticket.status === status) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (idUser && !status && date) {
        idUser = toNumbers(idUser);

        for (const ticket of this.tickets) {
          if ((ticket.idUser === idUser[0] || ticket.idUserDev === idUser[0]) &&
            new Date (ticket.created_at).toISOString().substr(0, 10) === date) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (idUser && !status && !date) {
        idUser = toNumbers(idUser);

        for (const ticket of this.tickets) {
          if ((ticket.idUser === idUser[0] || ticket.idUserDev === idUser[0])) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (!idUser && status && date) {

        for (const ticket of this.tickets) {
          if (ticket.status === status &&
            new Date (ticket.created_at).toISOString().substr(0, 10) === date) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (!idUser && status && !date) {
        for (const ticket of this.tickets) {
          if (ticket.status === status) {
            this.ticketFiltred.push(ticket);
          }
        }
      }

      if (!idUser && !status && date) {

        for (const ticket of this.tickets) {
          if ( new Date (ticket.created_at).toISOString().substr(0, 10) === date) {
            this.ticketFiltred.push(ticket);
          }
        }
      }
    } else {
      this.toastr.error('Veuillez saisir au moins 1 filtre.', 'Erreur');

    }
  }

  deleteFilter() {
    this.filtred = false;
    this.ticketFiltred = [];
    this.ngOnInit();
    // @ts-ignore
    document.getElementById('selectStatus').value = '';
    // @ts-ignore
    document.getElementById('calendar').value = '';
  }

  getMaterialName(idMaterial) {
    const str = '';
    for (const i in this.materials) {
      if (this.materials[i].id === idMaterial) {
        // @ts-ignore
        str = this.materials[i].name + '';
      }
    }
    return str;
  }

}
