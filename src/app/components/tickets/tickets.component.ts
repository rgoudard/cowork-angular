import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {Ticket} from '../../models/Ticket';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {tick} from '@angular/core/testing';
import {Material} from '../../models/Material';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html'
})
export class TicketsComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private tickets: Ticket[];
  private materials: Material[];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/ticketNotAffected')
      .subscribe((response: { tickets: Ticket[]}) => {
        console.log(response);
        if (response.tickets.length > 0 ) {
          this.tickets = response.tickets;
        } else {
          console.log('Pas de ticket non affectés');
        }
      });
    this.httpClient.get(this.url + '/material')
      .subscribe((response: { material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.materials = response.material;
        } else {
          console.log('Erreur de récupération des matériaux');
        }
      });
  }

  takeTicket(ticketId) {
    this.httpClient.put(this.url + '/tickets/' + ticketId, {
      idUserDev: this.user.id
    })
      .subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Ce ticket vous est désormais affecté.', 'Succès');
          return this.router.navigate(['/ticket/' + ticketId]);
        } else {
          this.toastr.error('Impossible d\'affecter ce ticket.', 'Erreur');
        }
      });
  }

  getMaterialName(idMaterial) {
    const str = '';
    for (const i in this.materials) {
      if (this.materials[i].id === idMaterial) {
        // @ts-ignore
        str = this.materials[i].name + '';
      }
    }
    return str;
  }
}
