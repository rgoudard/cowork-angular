import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Site} from '../../models/Site';
import {SiteHours} from '../../models/SiteHours';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Material} from '../../models/Material';
import {DatePipe} from '@angular/common';
import {delayResponse} from 'angular-in-memory-web-api/delay-response';
import {Reservation} from '../../models/Reservation';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html'
})
export class ReservationComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private sites: Site[];
  private dateBlock = new Date();
  private siteId: number;
  private siteHours: SiteHours[];
  private date;
  private dayHour: SiteHours;
  private isSelected: boolean;
  private dateIsChanged: boolean;
  private startChanged: boolean;
  private endChanged: boolean;
  private materials: Material[];
  private searchLaunched: boolean;
  private start;
  private end;
  private creationLaunched: boolean;
  private siteName;
  private materialId: number;
  private isCheck: boolean;

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService, private datePipe: DatePipe) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.dateBlock.setHours(this.dateBlock.getHours() + 24);
    this.httpClient.get(this.url + '/sites/')
      .subscribe((response: { sites: Site[] }) => {
        if (response.sites.length > 0) {
          this.sites = response.sites;
        } else {
          console.log('NO DATA FOUND');
        }
      });
  }

  onSelect(val) {
    this.siteId = val;
    for (const site of this.sites) {
      if (val === this.siteId) {
        this.siteName = site.name;
      }
    }
    this.httpClient.get(this.url + '/siteHours/' + val)
      .subscribe((response: { siteHours: SiteHours[] }) => {
        if (response.siteHours.length > 0) {
          this.siteHours = response.siteHours;
        }
      });
    this.isSelected = true;
    if (this.dateIsChanged) {
      // @ts-ignore
      this.onChangeDate(document.getElementById('calendar').value);
    }
  }

  onChangeDate(date) {
    this.dateIsChanged = true;
    this.date = new Date(date);
    let dateDay = this.date.getDay() - 1;
    if (dateDay === -1) {
      dateDay = 6;
    }
    this.dayHour = this.siteHours[dateDay];
    const open = this.dayHour.open;
    const close = this.dayHour.close;
    document.getElementById('start').setAttribute('min', open);
    document.getElementById('start').setAttribute('max', close);
    document.getElementById('end').setAttribute('min', open);
    document.getElementById('end').setAttribute('max', close);
  }

  startChange() {
    this.startChanged = true;
  }

  endChange() {
    this.endChanged = true;
  }

  searchMaterials(start, end) {

    this.start = start + ':00';
    this.end = end + ':00';
    let dateDay = this.date.getDay() - 1;
    if (dateDay === -1) {
      dateDay = 6;
    }

    const startDate = this.datePipe.transform(this.date, 'yyyy-MM-dd') + ' ' + this.start;
    const endDate = this.datePipe.transform(this.date, 'yyyy-MM-dd') + ' ' + this.end;

    if (this.date > new Date()) {
      if (this.start < this.end) {
        if (this.start >= this.siteHours[dateDay].open) {
          if (this.end <= this.siteHours[dateDay].close) {
            this.httpClient.post(this.url + '/freeReservableMaterial/' + this.siteId , {
              start: startDate,
              end: endDate
            }).subscribe((response: { materials: Material[] }) => {
                if (response.materials.length > 0) {
                  this.materials = response.materials;
                }
              });
            this.searchLaunched = true;
          } else {
            this.toastr.error('L\'heure de fin ne peut pas être après la fermeture.', 'Erreur');
          }
        } else {
          this.toastr.error('L\'heure de début ne peut pas être avant l\'ouverture.', 'Erreur');
        }
      } else {
        this.toastr.error('L\'heure de fin doit être supérieure à l\'heure de début.', 'Erreur');
      }
    } else {
      this.toastr.error('La date doit être plus élevée que la date du jour.', 'Erreur');
    }
  }

  launchCreation(materialId) {
    this.materialId = materialId;
    this.creationLaunched = true;
  }

  createReservation(materialId) {
    const startDate = this.datePipe.transform(this.date, 'yyyy-MM-dd') + ' ' + this.start;
    const endDate = this.datePipe.transform(this.date, 'yyyy-MM-dd') + ' ' + this.end;

    this.httpClient.post(this.url + '/reservation', {
      startDate,
      endDate,
      idMaterial: materialId,
      idUser: this.user.id
    }).subscribe((response: { reservation: Reservation, success: boolean }) => {
      if (response.success) {
        this.toastr.success('Reservation effectuée', 'Succès');
        return this.router.navigate(['/home']);
      } else {
        this.toastr.error('La reservation n \'a pas fonctionée.', 'Erreur');
      }
    });
  }

  isChecked(check) {
    if (check === true) {
      this.isCheck = true;
    } else {
      this.isCheck = false;
    }
  }
}
