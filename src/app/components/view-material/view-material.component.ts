import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {Material} from '../../models/Material';
import {Site} from '../../models/Site';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Reservation} from '../../models/Reservation';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

@Component({
  selector: 'app-view-material',
  templateUrl: './view-material.component.html'
})
export class ViewMaterialComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private materials: Material[];
  private sites: Site[];
  private filtred = false;
  private materialFiltred: Array<Material> = [];


  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/material')
      .subscribe((response: {material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.materials = response.material;
        } else {
          console.log('Erreur de récupération du matériel');
        }
      });

    this.httpClient.get(this.url + '/sites')
      .subscribe((response: {sites: Site[]}) => {
        if (response.sites.length > 0 ) {
          this.sites = response.sites;
        } else {
          console.log('Erreur de récupération des sites');
        }
      });
  }

  activeFilter(type, reservable, defect, site) {
    if (type || reservable || defect || site) {
      this.filtred = true;
      this.materialFiltred = [];


      if (type && reservable && defect && site) {
        site = toNumbers(site);
        reservable = toNumbers(reservable);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.type === type &&
              material.reservable === reservable[0] &&
              material.defect === defect[0] &&
              material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && reservable && defect && !site) {
        reservable = toNumbers(reservable);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.reservable === reservable[0] &&
            material.defect === defect[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && reservable && !defect && !site) {
        reservable = toNumbers(reservable);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.reservable === reservable[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && !reservable && defect && site) {
        site = toNumbers(site);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.defect === defect[0] &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && reservable && !defect && site) {
        site = toNumbers(site);
        reservable = toNumbers(reservable);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.reservable === reservable[0] &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && !reservable && defect && !site) {
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.defect === defect[0] ) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && !reservable && !defect && site) {
        site = toNumbers(site);

        for (const material of this.materials) {
          if ( material.type === type &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && reservable && defect && site) {
        site = toNumbers(site);
        reservable = toNumbers(reservable);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.reservable === reservable[0] &&
            material.defect === defect[0] &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && reservable && defect && !site) {
        reservable = toNumbers(reservable);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.reservable === reservable[0] &&
            material.defect === defect[0] ) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && reservable && !defect && site) {
        site = toNumbers(site);
        reservable = toNumbers(reservable);

        for (const material of this.materials) {
          if ( material.reservable === reservable[0] &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && !reservable && defect && site) {
        site = toNumbers(site);
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.defect === defect[0] &&
            material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (type && !reservable && !defect && !site) {
        for (const material of this.materials) {
          if ( material.type === type) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && reservable && !defect && !site) {
        reservable = toNumbers(reservable);

        for (const material of this.materials) {
          if ( material.reservable === reservable[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && !reservable && defect && !site) {
        defect = toNumbers(defect);

        for (const material of this.materials) {
          if ( material.defect === defect[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

      if (!type && !reservable && !defect && site) {
        site = toNumbers(site);

        for (const material of this.materials) {
          if ( material.idSite === site[0]) {
            this.materialFiltred.push(material);
          }
        }
      }

    } else {
      this.toastr.error('Veuillez saisir au moins 1 filtre.', 'Erreur');

    }
  }

  deleteFilter() {
    this.filtred = false;
    this.materialFiltred = [];
    this.ngOnInit();
    // @ts-ignore
    document.getElementById('selectType').value = '';
    // @ts-ignore
    document.getElementById('selectReservable').value = '';
    // @ts-ignore
    document.getElementById('selectDefect').value = '';
  }

  getMaterialType(type) {
    if (type === 'SR') {
      return 'Salle de réunion';
    }
    if (type === 'SA') {
      return 'Salle d\'appel';
    }
    if (type === 'Salon') {
      return 'Salon cosy';
    }
    if (type === 'Imprimante') {
      return 'Imprimante';
    }
    if (type === 'Ordinateur') {
      return 'Ordinateur';
    }
  }

  getMaterialBool(bool) {
    if (bool === 0) {
      return 'Non';
    } else {
      return 'Oui';
    }
  }

  getSiteName(idSite) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.sites) {
      if (this.sites[i].id === idSite) {
        str += this.sites[i].name;
      }
    }
    return str;
  }

}
