import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {SubscriptionType} from '../../models/subscriptionType';
import {ToastrService} from 'ngx-toastr';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html'
})
export class SubscriptionComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private subscriptionTypes: SubscriptionType[];
  public payPalConfig?: IPayPalConfig;
  private subscribe = false;
  private showSuccess = false;
  private subscriptionType: SubscriptionType;
  private agreement = false;

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/subscriptionType')
      .subscribe((response: { subscriptionType: SubscriptionType[] }) => {
        if (response.subscriptionType.length > 0 ) {
          this.subscriptionTypes = response.subscriptionType;
        } else {
          console.log('Erreur de récupération des types d\'abonnement');
        }
      });
  }

  subscription(idSubscriptionType, agreementTime) {
    for (const subscriptionType of this.subscriptionTypes) {
      if (subscriptionType.id === idSubscriptionType) {
        this.subscriptionType = subscriptionType;
      }
    }
    if ( agreementTime > 1 ){
      this.initConfig(this.subscriptionType.agreementPrice - 0.5, this.subscriptionType.name, idSubscriptionType, agreementTime);
      this.agreement = true;
    } else {
      this.initConfig(this.subscriptionType.subscriptionPrice + 0.0, this.subscriptionType.name, idSubscriptionType, agreementTime);
    }

    this.subscribe = true;
  }

  private initConfig(price, name, idSubscriptionType, agreementTime): void {
    this.payPalConfig = {
      currency: 'EUR',
      clientId: 'AXcwp86p8JB3VuRHBhg_Z9IW6wJsx1i9Gth_rfX1wgegAp61OvjVwvN5qR0833s57pLn-TZwACBWG5Wg',
      createOrderOnClient: (data) => <ICreateOrderRequest> {
        intent: 'CAPTURE',
        purchase_units: [
          {
            amount: {
              currency_code: 'EUR',
              value: price,
              breakdown: {
                item_total: {
                  currency_code: 'EUR',
                  value: price
                }
              }
            },
            items: [
              {
                name: name,
                quantity: '1',
                category: 'DIGITAL_GOODS',
                unit_amount: {
                  currency_code: 'EUR',
                  value: price
                },
              }
            ]
          }
        ]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical'
      },
      onApprove: (data, actions) => {
        console.log('onApprove - transaction was approved, but not authorized', data, actions);
        actions.order.get().then(details => {
          console.log('onApprove - you can get full order details inside onApprove: ', details);
        });
      },
      onClientAuthorization: (data) => {
        console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
        this.httpClient.post(this.url + '/subscription', {
          agreementTime,
          idUser: this.user.id,
          idSubscriptionType
        }).subscribe((response: {success: boolean}) => {
          console.log(response);
          if (response.success) {
            this.toastr.success('Abonnement effectué', 'Succès');
            return this.router.navigate(['/home']);
          } else {
            this.toastr.error('Abonnement impossible', 'Erreur');
          }
        });
      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
      },
      onError: err => {
        console.log('OnError', err);
      },
      onClick: (data, actions) => {
        console.log('onClick', data, actions);
      },
    };
  }

  cancel() {
    this.subscribe = false;
    this.agreement = false;
  }
}
