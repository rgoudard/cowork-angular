import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Site} from '../../models/Site';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';
import {Orders} from '../../models/Orders';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html'
})
export class ViewOrderComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private orders: Orders[];
  private users: User[];
  private sites: Site[];
  private filtred = false;
  private orderFiltred: Array<Orders> = [];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {

    this.httpClient.get(this.url + '/order')
      .subscribe((response: {orders: Orders[]}) => {
      if (response.orders.length > 0 ) {
        this.orders = response.orders;
      } else {
        console.log('Erreur de récupération des commandes');
      }
    });

    this.httpClient.get(this.url + '/users')
      .subscribe((response: {users: User[]}) => {
        if (response.users.length > 0 ) {
          this.users = response.users;
        } else {
          console.log('Erreur de récupération des utilisateurs');
        }
      });

    this.httpClient.get(this.url + '/sites')
      .subscribe((response: {sites: Site[]}) => {
      if (response.sites.length > 0 ) {
        this.sites = response.sites;
      } else {
        console.log('Erreur de récupération des sites');
      }
    });
  }

  getUserName(idUser) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.users) {
      if (this.users[i].id === idUser) {
        str += this.users[i].firstName + ' ' + this.users[i].lastName;
      }
    }
    return str;
  }

  getSiteName(idSite) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.sites) {
      if (this.sites[i].id === idSite) {
        str += this.sites[i].name;
      }
    }
    return str;
  }

  activeFilter(idUser, idSite, date) {
    if (idUser || idSite || date) {
      this.filtred = true;
      this.orderFiltred = [];

      if (idUser && idSite && date) {
        idUser = toNumbers(idUser);
        idSite = toNumbers(idSite);

        for (const order of this.orders) {
          if (order.idUser === idUser[0] &&
            order.idSite === idSite[0] &&
            new Date (order.date).toISOString().substr(0, 10) === date) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (idUser && idSite && !date) {
        idUser = toNumbers(idUser);
        idSite = toNumbers(idSite);

        for (const order of this.orders) {
          if (order.idUser === idUser[0] &&
            order.idSite === idSite[0]) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (idUser && !idSite && date) {
        idUser = toNumbers(idUser);

        for (const order of this.orders) {
          if (order.idUser === idUser[0] &&
            new Date (order.date).toISOString().substr(0, 10) === date) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (idUser && !idSite && !date) {
        idUser = toNumbers(idUser);

        for (const order of this.orders) {
          if (order.idUser === idUser[0]) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (!idUser && idSite && date) {
        idSite = toNumbers(idSite);

        for (const order of this.orders) {
          if (order.idSite === idSite[0] &&
            new Date (order.date).toISOString().substr(0, 10) === date) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (!idUser && idSite && !date) {
        idSite = toNumbers(idSite);

        for (const order of this.orders) {
          if (order.idSite === idSite[0]) {
            this.orderFiltred.push(order);
          }
        }
      }

      if (!idUser && !idSite && date) {

        for (const order of this.orders) {
          if ( new Date (order.date).toISOString().substr(0, 10) === date) {
            this.orderFiltred.push(order);
          }
        }
      }
    } else {
      this.toastr.error('Veuillez saisir au moins 1 filtre.', 'Erreur');

    }
  }

  deleteFilter() {
    this.filtred = false;
    this.orderFiltred = [];
    this.ngOnInit();
    // @ts-ignore
    document.getElementById('calendar').value = '';
  }
}
