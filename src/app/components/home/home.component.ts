import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/User';
import {Reservation} from '../../models/Reservation';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Material} from '../../models/Material';
import {Site} from '../../models/Site';
// @ts-ignore
import {Subscription} from '../../models/Subscription';
import {SubscriptionType} from '../../models/subscriptionType';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  private user: User;
  private reservations: Reservation[];
  private url = 'http://51.91.10.171:3000';
  private materialList: Array<Material> = [];
  private siteList: Array<Site> = [];
  private index: Array<number> = [];
  private currentSubscription: Subscription;
  private currentSubscriptionType: SubscriptionType;
  private needSubscription = false;

  constructor(private userService: UserService, private httpClient: HttpClient, private router: Router) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/reservation/user/' + this.user.id)
    .subscribe((response: {reservations: Reservation[], length: number, success: boolean}) => {
      if (response.length > 0) {
        this.reservations = response.reservations;
        for (let i = 0; i < this.reservations.length; i++) {
          this.index.push(i);
        }
        for (const reservation of this.reservations) {
          this.getMaterialName(reservation.idMaterial);
        }
      } else {
        console.log('NO DATA FOUND');
      }
    });

    this.httpClient.get(this.url + '/subscription/' + this.user.id)
      .subscribe((response: {subscription: Subscription[]}) => {
        if (response.subscription.length > 0 ) {
          this.currentSubscription = response.subscription[0];
          this.httpClient.get(this.url + '/subscriptionType/' + this.currentSubscription.idSubscriptionType)
            .subscribe((responseST: {subscriptionType: SubscriptionType[]}) => {
              if (responseST.subscriptionType.length > 0) {
                this.currentSubscriptionType = responseST.subscriptionType[0];
              } else {
                console.log('NO DATA FOUND');
              }
            });
          const date = new Date();
          if ( new Date(date.setDate(date.getDate() + 7))  >= new Date(this.currentSubscription.endDate)) {
            this.needSubscription = true;
          }
        } else {
          this.httpClient.get(this.url + '/subscriptionType/' + 1)
            .subscribe((responseST: {subscriptionType: SubscriptionType[]}) => {
              if (responseST.subscriptionType.length > 0) {
                this.currentSubscriptionType = responseST.subscriptionType[0];
              } else {
                console.log('NO DATA FOUND');
              }
            });
        }
      });
  }

  goToReservation() {
    return this.router.navigate(['/reservation']);
  }

  goToSubscription() {
    return this.router.navigate(['/subscription']);
  }

  getMaterialName(idMaterial) {
    this.httpClient.get(this.url + '/material/' + idMaterial)
      .subscribe((response: { material: Material[]}) => {
        this.materialList.push(response.material[0]);
        this.getSiteName(response.material[0].idSite);
      });
  }

  getSiteName(idSite) {
    this.httpClient.get(this.url + '/sites/' + idSite)
      .subscribe((response: { site: Site[]}) => {
        this.siteList.push(response.site[0]);
      });
  }
}
