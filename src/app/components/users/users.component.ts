import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
// @ts-ignore
import {Subscription} from '../../models/Subscription';
import {SubscriptionType} from '../../models/subscriptionType';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private users: User[];
  private currentSubscriptions: Subscription[];
  private subscriptionTypes: SubscriptionType[];
  private filtred = false;
  private userFiltred: Array<User> = [];


  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/users')
      .subscribe((response: {users: User[]}) => {
        if (response.users.length > 0 ) {
          this.users = response.users;
        } else {
          console.log('Erreur de récupération des utilisateurs');
        }
      });

    this.httpClient.get(this.url + '/currentSubscription')
      .subscribe((response: {subscription: Subscription[]}) => {
        if (response.subscription.length > 0) {
          this.currentSubscriptions = response.subscription;
        } else {
          console.log('Erreur de récupération des abonnements en cours');
        }
      });

    this.httpClient.get(this.url + '/subscriptionType')
      .subscribe((response: {subscriptionType: SubscriptionType[]}) => {
        if (response.subscriptionType.length > 0 ) {
          this.subscriptionTypes = response.subscriptionType;
        } else {
          console.log('Erreur de récupération des types d\'abonnement');
        }
      });
  }

  onChangeType(type, userId) {
    this.httpClient.put(this.url + '/manageUserType/' + userId, {
      type
    })
      .subscribe((response: { success: boolean}) => {
        if (response.success) {
          this.toastr.success('Type d\'utilisateur modifié.', 'Succès');
        } else {
          this.toastr.error('Impossible de modifier le type de l\'utilisateur.', 'Erreur');
        }
      });
  }

  getSubscriptionName(idUser) {
    let subscriptionName = '';
    // tslint:disable-next-line:forin
    for (const i in this.currentSubscriptions) {
      if (this.currentSubscriptions[i].idUser === idUser) {
        const idSubscriptionType = this.currentSubscriptions[i].idSubscriptionType;
        for (const y in this.subscriptionTypes) {
          if (this.subscriptionTypes[y].id === idSubscriptionType) {
            subscriptionName = this.subscriptionTypes[y].name;
          }
        }
      }
    }
    return subscriptionName;
  }

  getSubscriptionDate(idUser , position) {
    let date: Date;
    // tslint:disable-next-line:forin
    for (const i in this.currentSubscriptions) {
      if (this.currentSubscriptions[i].idUser === idUser) {
        if (position === 'start') {
          date = this.currentSubscriptions[i].startDate;
        } else if (position === 'end') {
          date = this.currentSubscriptions[i].endDate;
        }
      }
    }
    return date;
  }

  activeFilter(idUser, userType, idSubscriptionType) {
    if (idUser || userType || idSubscriptionType) {
      this.filtred = true;
      this.userFiltred = [];

      if (idUser && userType && idSubscriptionType) {
        idUser = toNumbers(idUser);
        idSubscriptionType = toNumbers(idSubscriptionType);

        for (const user of this.users) {
          if (user.id === idUser[0] &&
            user.type === userType) {

            let count = 0;

            if (idSubscriptionType[0] === 1) {
              count = 0;
              for (const currentSubscription of this.currentSubscriptions) {
                if (user.id !== currentSubscription.idUser) {
                  count ++;
                }
              }
              if (count === this.currentSubscriptions.length) {
                this.userFiltred.push(user);
              }
            }

            for (const subscription of this.currentSubscriptions) {
              if (subscription.idSubscriptionType === idSubscriptionType[0] && user.id === subscription.idUser) {
                this.userFiltred.push(user);
              }
            }
          }
        }
      }

      if (idUser && userType && !idSubscriptionType) {
        idUser = toNumbers(idUser);

        for (const user of this.users) {
          if (user.id === idUser[0] &&
            user.type === userType) {
            this.userFiltred.push(user);
          }
        }
      }

      if (idUser && !userType && idSubscriptionType) {
        idUser = toNumbers(idUser);
        idSubscriptionType = toNumbers(idSubscriptionType);

        for (const user of this.users) {
          if (user.id === idUser[0] ) {
            let count = 0;

            if (idSubscriptionType[0] === 1) {
              count = 0;
              for (const currentSubscription of this.currentSubscriptions) {
                if (user.id !== currentSubscription.idUser) {
                  count ++;
                }
              }
              if (count === this.currentSubscriptions.length) {
                this.userFiltred.push(user);
              }
            }

            for (const subscription of this.currentSubscriptions) {
              if (subscription.idSubscriptionType === idSubscriptionType[0] && user.id === subscription.idUser) {
                this.userFiltred.push(user);
              }
            }
          }
        }
      }

      if (idUser && !userType && !idSubscriptionType) {
        idUser = toNumbers(idUser);
        idSubscriptionType = toNumbers(idSubscriptionType);

        for (const user of this.users) {
          if (user.id === idUser[0]) {
            this.userFiltred.push(user);
          }
        }
      }

      if (!idUser && userType && idSubscriptionType) {
        idSubscriptionType = toNumbers(idSubscriptionType);

        for (const user of this.users) {
          if (user.type === userType) {

            let count = 0;

            if (idSubscriptionType[0] === 1) {
              count = 0;
              for (const currentSubscription of this.currentSubscriptions) {
                if (user.id !== currentSubscription.idUser) {
                  count ++;
                }
              }
              if (count === this.currentSubscriptions.length) {
                this.userFiltred.push(user);
              }
            }

            for (const subscription of this.currentSubscriptions) {
              if (subscription.idSubscriptionType === idSubscriptionType[0] && user.id === subscription.idUser) {
                this.userFiltred.push(user);
              }
            }
          }
        }
      }

      if (!idUser && userType && !idSubscriptionType) {
        for (const user of this.users) {
          if (user.type === userType) {
            this.userFiltred.push(user);
          }
        }
      }

      if (!idUser && !userType && idSubscriptionType) {
        idSubscriptionType = toNumbers(idSubscriptionType);

        let count = 0;

        if (idSubscriptionType[0] === 1) {
          for (const user of this.users) {
            count = 0;
            for (const currentSubscription of this.currentSubscriptions) {
              if (user.id !== currentSubscription.idUser) {
                count ++;
              }
            }
            if (count === this.currentSubscriptions.length) {
              this.userFiltred.push(user);
            }
          }
        }

        for (const user of this.users) {
          for (const subscription of this.currentSubscriptions) {
            if (subscription.idSubscriptionType === idSubscriptionType[0] && user.id === subscription.idUser) {
              this.userFiltred.push(user);
            }
          }
        }
      }
    } else {
      this.toastr.error('Veuillez saisir au moins 1 filtre.', 'Erreur');

    }
  }

  deleteFilter() {
    this.filtred = false;
    this.userFiltred = [];
    this.ngOnInit();
    // @ts-ignore
    document.getElementById('selectUserType').value = '';
  }
}
