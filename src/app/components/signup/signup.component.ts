import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(lastname: string, firstname: string, email: string, password: string, repeatPaswword: string) {
    this.userService.createUser(lastname, firstname, email, password, repeatPaswword);
  }

}
