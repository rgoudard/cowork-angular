import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Reservation} from '../../models/Reservation';
import {Material} from '../../models/Material';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

@Component({
  selector: 'app-view-reservation',
  templateUrl: './view-reservation.component.html'
})
export class ViewReservationComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private reservation: Reservation[];
  private users: User[];
  private material: Material[];
  private filtred = false;
  private reservationFiltred: Array<Reservation> = [];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/reservation')
      .subscribe((response: {reservations: Reservation[]}) => {
        if (response.reservations.length > 0 ) {
          this.reservation = response.reservations;
        } else {
          console.log('Erreur de récupération des réservations');
        }
      });

    this.httpClient.get(this.url + '/users')
      .subscribe((response: {users: User[]}) => {
        if (response.users.length > 0 ) {
          this.users = response.users;
        } else {
          console.log('Erreur de récupération des utilisateurs');
        }
      });

    this.httpClient.get(this.url + '/material')
      .subscribe((response: { material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.material = response.material;
        } else {
          console.log('Erreur de récupération des matériaux');
        }
      });
  }

  getUserName(idUser) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.users) {
      if (this.users[i].id === idUser) {
          str += this.users[i].firstName + ' ' + this.users[i].lastName;
        }
    }
    return str;
  }

  getMaterialName(idMaterial) {
    let str = '';
    // tslint:disable-next-line:forin
    for (const i in this.material) {
      if (this.material[i].id === idMaterial) {
        str += this.material[i].name;
      }
    }
    return str;
  }

  activeFilter(idUser, idMaterial, date) {
    if (idUser || idMaterial || date) {
      this.filtred = true;
      this.reservationFiltred = [];

      if (idUser && idMaterial && date) {
        idUser = toNumbers(idUser);
        idMaterial = toNumbers(idMaterial);

        for (const reservations of this.reservation) {
          if (reservations.idUser === idUser[0] &&
              reservations.idMaterial === idMaterial[0] &&
              new Date (reservations.startDate).toISOString().substr(0, 10) === date) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (idUser && idMaterial && !date) {
        idUser = toNumbers(idUser);
        idMaterial = toNumbers(idMaterial);

        for (const reservations of this.reservation) {
          if (reservations.idUser === idUser[0] &&
            reservations.idMaterial === idMaterial[0]) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (idUser && !idMaterial && date) {
        idUser = toNumbers(idUser);

        for (const reservations of this.reservation) {
          if (reservations.idUser === idUser[0] &&
            new Date (reservations.startDate).toISOString().substr(0, 10) === date) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (idUser && !idMaterial && !date) {
        idUser = toNumbers(idUser);

        for (const reservations of this.reservation) {
          if (reservations.idUser === idUser[0]) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (!idUser && idMaterial && date) {
        idMaterial = toNumbers(idMaterial);

        for (const reservations of this.reservation) {
          if (reservations.idMaterial === idMaterial[0] &&
            new Date (reservations.startDate).toISOString().substr(0, 10) === date) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (!idUser && idMaterial && !date) {
        idMaterial = toNumbers(idMaterial);

        for (const reservations of this.reservation) {
          if (reservations.idMaterial === idMaterial[0]) {
            this.reservationFiltred.push(reservations);
          }
        }
      }

      if (!idUser && !idMaterial && date) {

        for (const reservations of this.reservation) {
          if ( new Date (reservations.startDate).toISOString().substr(0, 10) === date) {
            this.reservationFiltred.push(reservations);
          }
        }
      }
    } else {
      this.toastr.error('Veuillez saisir au moins 1 filtre.', 'Erreur');

    }
  }

  deleteFilter() {
    this.filtred = false;
    this.reservationFiltred = [];
    this.ngOnInit();
    // @ts-ignore
    document.getElementById('calendar').value = '';
  }

}
