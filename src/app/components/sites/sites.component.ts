import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Site} from '../../models/Site';
import {HttpClient} from '@angular/common/http';
import {SiteHours} from '../../models/SiteHours';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html'
})
export class SitesComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private sites: Site[];
  private siteHours: SiteHours[];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/sites/')
      .subscribe((response: { sites: Site[] }) => {
        if (response.sites.length > 0) {
          this.sites = response.sites;
        } else {
          console.log('NO DATA FOUND');
        }
      });

    this.httpClient.get(this.url + '/siteHours')
      .subscribe((response: { siteHours: SiteHours[] }) => {
        if (response.siteHours.length > 0) {
          this.siteHours = response.siteHours;
        }
      });
  }

  siteAccess(siteId) {
    return this.router.navigate(['/sites/' + siteId]);
  }

  createSite(name, address) {
    if (name) {
      if (address) {
        this.httpClient.post(this.url + '/sites', {
          name,
          address,
          hightWiFi: false,
          trays: false,
          illimitedCons: false
        }).subscribe((response: { site: number; success: boolean}) => {
          this.toastr.success('Votre site a bien été créé.', 'Succès');
          if (response.success) {
            for (let i = 0; i < 7; i++) {
              let day = '';
              if (i === 0 ) {
                day = 'Lundi';
              }
              if (i === 1 ) {
                day = 'Mardi';
              }
              if (i === 2 ) {
                day = 'Mercredi';
              }
              if (i === 3 ) {
                day = 'Jeudi';
              }
              if (i === 4 ) {
                day = 'Vendredi';
              }
              if (i === 5 ) {
                day = 'Samedi';
              }
              if (i === 6 ) {
                day = 'Dimanche';
              }
              this.httpClient.post(this.url + '/siteHours', {
                day,
                open: '09:00:00',
                close: '20:00:00',
                idSite: response.site
              }).subscribe((res: {success: boolean}) => {
                if (res.success) {
                  this.ngOnInit();
                } else {
                  this.toastr.error('Impossible de créer les horraires du site.', 'Erreur');
                }
              });
            }
          } else {
            this.toastr.error('Impossible de créer le site.', 'Erreur');
          }
        });
      } else {
        this.toastr.error('Le champ "Adresse" doit être renseigné', 'Erreur');
      }
    } else {
      this.toastr.error('Le champ "Nom" doit être renseigné', 'Erreur');
    }
  }

}
