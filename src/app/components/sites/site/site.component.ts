import { Component, OnInit } from '@angular/core';
import {User} from '../../../models/User';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Site} from '../../../models/Site';
import {SiteHours} from '../../../models/SiteHours';
import {Material} from '../../../models/Material';
import {RequestOptions} from '@angular/http';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html'
})
export class SiteComponent implements OnInit {

  private siteId;
  private user: User;
  private url = 'http://51.91.10.171:3000';
  private site: Site;
  private siteHours: SiteHours[];
  private isCheck = false;
  private deleted = false;
  private countSR = 0;
  private countSA = 0;
  private countSalon = 0;
  private countPrinter = 0;
  private countComputer = 0;

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService, private route: ActivatedRoute) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.siteId = this.route.snapshot.paramMap.get('siteId');
    this.httpClient.get(this.url + '/sites/' + this.siteId)
      .subscribe((response: {site: Site[]}) => {
        if (response.site.length > 0) {
          this.site = response.site[0];
        } else {
          console.log('Impossible de trouver le site');
        }
      });
    this.httpClient.get(this.url + '/siteHours/' + this.siteId)
      .subscribe((response: { siteHours: SiteHours[] }) => {
        if (response.siteHours.length > 0) {
          this.siteHours = response.siteHours;
        }
      });
    this.httpClient.get(this.url + '/material/site/' + this.siteId)
      .subscribe((response: {material: Material[]}) => {
        for (let i = 0; i < response.material.length; i++) {
          if (response.material[i].type === 'SR') { this.countSR += 1; }
          if (response.material[i].type === 'SA') { this.countSA += 1; }
          if (response.material[i].type === 'Salon') { this.countSalon += 1; }
          if (response.material[i].type === 'Imprimante') { this.countPrinter += 1; }
          if (response.material[i].type === 'Ordinateur') { this.countComputer += 1; }
        }
      });
  }

  updateSite() {
    // @ts-ignore
    const name = document.getElementById('inputName').value;
    // @ts-ignore
    const address = document.getElementById('inputAddress').value;
    // @ts-ignore
    const hightWiFi = document.getElementById('hightWiFi').checked;
    // @ts-ignore
    const trays = document.getElementById('trays').checked;
    // @ts-ignore
    const illimitedCons = document.getElementById('illimitedCons').checked;

    if (name && address) {
      this.httpClient.put(this.url + '/sites/' + this.siteId, {
        name,
        address,
        hightWiFi,
        trays,
        illimitedCons
      }).subscribe((response: {affectedRows: number}) => {
        if (response.affectedRows === 1) {
          this.toastr.success('Modification effectuée', 'Succès');
        }
      });
    } else {
      this.toastr.error('Le nom et l\'adresse sont obligatoires', 'Erreur');
    }
  }

  deleteSite() {
    this.deleted = true;
  }

  isChecked(check) {
    if (check === true) {
      this.isCheck = true;
    } else {
      this.isCheck = false;
    }
  }

  onRemove() {
    this.httpClient.delete(this.url + '/sites/' + this.siteId)
      .subscribe((response: { success: boolean}) => {
        if (response.success) {
          this.toastr.success('Le site a bien été supprimé.', 'Succès');
          return this.router.navigate(['/sites']);
        } else {
          this.toastr.error('Impossible de supprimer.', 'Erreur');
        }
      });
  }

  onChangeStartHour(open, close, day, idSite, id) {
    this.httpClient.put(this.url + '/siteHours/' + id, {
      day,
      open,
      close,
      idSite
    }).subscribe((response: {affectedRows: number}) => {
      if (response.affectedRows === 1) {
        this.toastr.success('L\'horraire a bien été modifiée', 'Succès');
      }
    });
  }

  onChangeEndHour(close, open, day, idSite, id) {
    this.httpClient.put(this.url + '/siteHours/' + id, {
      day,
      open,
      close,
      idSite
    }).subscribe((response: {affectedRows: number}) => {
      if (response.affectedRows === 1) {
        this.toastr.success('L\'horraire a bien été modifiée', 'Succès');
      } else {
        this.toastr.error('Impossible de modifier l\'horraire', 'Erreur');
      }
    });
  }

  createMaterial(type) {
    if (type === 'SR') {
      this.countSR += 1;
      this.httpClient.post(this.url + '/material', {
        type,
        name: 'Salle de réunion SR-' + this.countSR,
        reservable: true,
        defect: false,
        idSite: this.siteId
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Matériel correctement créé', 'Succès');
        } else {
          this.toastr.error('Impossible de créer le matériel', 'Erreur');
        }
      });
    }
    if (type === 'SA') {
      this.countSA += 1;
      this.httpClient.post(this.url + '/material', {
        type,
        name: 'Salle d\'appel SA-' + this.countSA,
        reservable: true,
        defect: false,
        idSite: this.siteId
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Matériel correctement créé', 'Succès');
        } else {
          this.toastr.error('Impossible de créer le matériel', 'Erreur');
        }
      });
    }
    if (type === 'Salon') {
      this.countSalon += 1;
      this.httpClient.post(this.url + '/material', {
        type,
        name: 'Salon cosy SC-' + this.countSalon,
        reservable: false,
        defect: false,
        idSite: this.siteId
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Matériel correctement créé', 'Succès');
        } else {
          this.toastr.error('Impossible de créer le matériel', 'Erreur');
        }
      });
    }
    if (type === 'Imprimante') {
      this.countPrinter += 1;
      this.httpClient.post(this.url + '/material', {
        type,
        name: 'Imprimante I-' + this.countPrinter,
        reservable: false,
        defect: false,
        idSite: this.siteId
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Matériel correctement créé', 'Succès');
        } else {
          this.toastr.error('Impossible de créer le matériel', 'Erreur');
        }
      });
    }
    if (type === 'Ordinateur') {
      this.countComputer += 1;
      this.httpClient.post(this.url + '/material', {
        type,
        name: 'Ordinateur portable OP-' + this.countComputer,
        reservable: true,
        defect: false,
        idSite: this.siteId
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Matériel correctement créé', 'Succès');
        } else {
          this.toastr.error('Impossible de créer le matériel', 'Erreur');
        }
      });
    }
  }

  deleteMaterial(type) {
    if (type === 'SR') {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          name: 'Salle de réunion SR-' + this.countSR,
          idSite: this.siteId
        },
      };
      this.httpClient.delete(this.url + '/material', options)
        .subscribe((response: {affectedRows: number}) => {
          console.log(response);
          if (response.affectedRows === 1) {
            this.toastr.success('Matériel correctement supprimé', 'Succès');
          } else {
            this.toastr.error('Impossible de supprimer le matériel', 'Erreur');
          }
        });
      this.countSR -= 1;
    }
    if (type === 'SA') {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          name: 'Salle d\'appel SA-' + this.countSA,
          idSite: this.siteId
        },
      };
      this.httpClient.delete(this.url + '/material', options)
        .subscribe((response: {affectedRows: number}) => {
          if (response.affectedRows === 1) {
            this.toastr.success('Matériel correctement supprimé', 'Succès');
          } else {
            this.toastr.error('Impossible de supprimer le matériel', 'Erreur');
          }
        });
      this.countSA -= 1;
    }
    if (type === 'Salon') {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          name: 'Salon cosy SC-' + this.countSalon,
          idSite: this.siteId
        },
      };
      this.httpClient.delete(this.url + '/material', options)
        .subscribe((response: {affectedRows: number}) => {
          if (response.affectedRows === 1) {
            this.toastr.success('Matériel correctement supprimé', 'Succès');
          } else {
            this.toastr.error('Impossible de supprimer le matériel', 'Erreur');
          }
        });
      this.countSalon -= 1;
    }
    if (type === 'Imprimante') {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          name: 'Imprimante I-' + this.countPrinter,
          idSite: this.siteId
        },
      };
      this.httpClient.delete(this.url + '/material', options)
        .subscribe((response: {affectedRows: number}) => {
          if (response.affectedRows === 1) {
            this.toastr.success('Matériel correctement supprimé', 'Succès');
          } else {
            this.toastr.error('Impossible de supprimer le matériel', 'Erreur');
          }
        });
      this.countPrinter -= 1;
    }
    if (type === 'Ordinateur') {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          name: 'Ordinateur portable OP-' + this.countComputer,
          idSite: this.siteId
        },
      };
      this.httpClient.delete(this.url + '/material', options)
        .subscribe((response: {affectedRows: number}) => {
          if (response.affectedRows === 1) {
            this.toastr.success('Matériel correctement supprimé', 'Succès');
          } else {
            this.toastr.error('Impossible de supprimer le matériel', 'Erreur');
          }
        });
      this.countComputer -= 1;
    }
  }

}
