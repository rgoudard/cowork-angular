import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../models/User';
import {Message} from '../../../models/Message';
import {Ticket} from '../../../models/Ticket';
import {Material} from '../../../models/Material';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html'
})
export class TicketComponent implements OnInit {

  private ticketId;
  private user: User;
  private url = 'http://51.91.10.171:3000';
  private messages: Message[];
  private tickets: Ticket[];
  private closed = false;
  private materials: Material[];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService, private route: ActivatedRoute) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.ticketId = this.route.snapshot.paramMap.get('ticketId');
    this.httpClient.get(this.url + '/ticket/' + this.ticketId)
      .subscribe((response: {ticket: Ticket[]}) => {
        if (response.ticket.length === 1) {
          this.tickets = response.ticket;
          // @ts-ignore
          if (this.tickets[0].status === 'CLOS') {
            this.closed = true;
          }
        }
      });
    this.httpClient.get(this.url + '/material')
      .subscribe((response: { material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.materials = response.material;
        } else {
          console.log('Erreur de récupération des matériaux');
        }
      });
    this.httpClient.get(this.url + '/messages/' + this.ticketId)
      .subscribe((response: {messages: Message[]}) => {
        if (response.messages.length > 0) {
          this.messages = response.messages;
        } else {
          console.log('Pas de message trouvés');
        }
      });
    const messageDiv = document.getElementById('messageDiv');
    messageDiv.scrollTop = messageDiv.scrollHeight;
  }

  sendMessage(message) {
    if (message) {
      this.httpClient.post(this.url + '/messages', {
        message,
        idUser: this.user.id,
        idTicket: this.ticketId
      }).subscribe((response: { success: boolean}) => {
        if (response.success) {
          this.ngOnInit();
        } else {
          console.log('Message non envoyé');
        }
      });
    }
  }

  closeTicket() {
    this.httpClient.put(this.url + '/closeTicket/' + this.ticketId, {})
      .subscribe((response: {success: boolean}) => {
        if (response.success) {
          if (this.tickets[0].idMaterial) {
            for (const material of this.materials) {
              if (this.tickets[0].idMaterial === material.id) {
                this.httpClient.put(this.url + '/material/' + this.tickets[0].idMaterial , {
                  type: material.type,
                  name: material.name,
                  reservable: material.reservable,
                  defect: 0,
                  idSite: material.idSite
                }).subscribe((responseUpdate: {material}) => {
                  if ( responseUpdate.material.affectedRows === 1) {
                    this.toastr.success('Le ticket est clos', 'SUCCES');
                    return this.router.navigate(['/mytickets']);
                  } else {
                    this.toastr.error('Impossible de modifier l\'état du matériel', 'Erreur');
                  }
                });
              }
            }
          }
        }
      });
  }

  getMaterialName(idMaterial) {
    const str = '';
    for (const i in this.materials) {
      if (this.materials[i].id === idMaterial) {
        // @ts-ignore
        str = this.materials[i].name + '';
      }
    }
    return str;
  }
}
