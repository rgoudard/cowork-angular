import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Ticket} from '../../models/Ticket';
import {Material} from '../../models/Material';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';

@Component({
  selector: 'app-my-tickets',
  templateUrl: './my-tickets.component.html'
})
export class MyTicketsComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private tickets: Ticket[];
  private materials: Material[];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/tickets/' + this.user.id)
      .subscribe((response: { tickets: Ticket[]}) => {
        if (response.tickets.length > 0 ) {
          this.tickets = response.tickets;
        } else {
          console.log('Pas de tickets');
        }
      });

    this.httpClient.get(this.url + '/material')
      .subscribe((response: { material: Material[]}) => {
        if (response.material.length > 0 ) {
          this.materials = response.material;
        } else {
          console.log('Erreur de récupération des matériaux');
        }
      });
  }

  createTicket(name, subject, type, idMaterial) {
    idMaterial = toNumbers(idMaterial);
    if (name) {
      if (subject) {
        if (type) {
          if ((type === 'Technique' && idMaterial[0]) || (type === 'Autre')) {
            this.httpClient.post(this.url + '/tickets', {
              title: name,
              subject,
              idUser: this.user.id,
              type,
              idMaterial: idMaterial[0]
            }).subscribe((response: { success: boolean }) => {
              if (response.success) {
                if (type === 'Technique') {
                  for (const material of this.materials) {
                      if (idMaterial[0] === material.id) {
                        this.httpClient.put(this.url + '/material/' + idMaterial[0] , {
                          type: material.type,
                          name: material.name,
                          reservable: material.reservable,
                          defect: 1,
                          idSite: material.idSite
                        }).subscribe((responseUpdate: {material}) => {
                          if ( responseUpdate.material.affectedRows === 1) {
                            this.toastr.success('Votre ticket a bien été créé.', 'Succès');
                            this.ngOnInit();
                          } else {
                            this.toastr.error('Impossible de modifier l\'état du matériel', 'Erreur');
                          }
                        });
                      }
                  }
                } else {
                  this.toastr.success('Votre ticket a bien été créé.', 'Succès');
                  this.ngOnInit();
                }
              } else {
                this.toastr.error('Impossible de créer le ticket.', 'Erreur');
              }
            });
          } else {
            this.toastr.error('Le champ "Matériel concerné" doit être renseigné pour les soucis technique', 'Erreur');
          }
        } else {
          this.toastr.error('Le champ "Type" doit être renseigné', 'Erreur');
        }
      } else {
        this.toastr.error('Le champ "Sujet" doit être renseigné', 'Erreur');
      }
    } else {
      this.toastr.error('Le champ "Nom" doit être renseigné', 'Erreur');
    }
  }

  goToTicket(ticketId) {
    return this.router.navigate(['/ticket/' + ticketId]);
  }

  getMaterialName(idMaterial) {
    const str = '';
    for (const i in this.materials) {
      if (this.materials[i].id === idMaterial) {
        // @ts-ignore
        str = this.materials[i].name + '';
      }
    }
    return str;
  }
}
