import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Site} from '../../models/Site';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Orders} from '../../models/Orders';
import {applySourceSpanToExpressionIfNeeded} from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {

  private url = 'http://51.91.10.171:3000';

  private user: User;
  private sites: Site[];
  private idSite: number;
  private dateBlock = new Date();
  private date;
  private siteSelected = false;
  private dateSelected = false;
  private orders: Orders[];
  private index: Array<number> = [];
  private orderSite: Array<Site> = [];


  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.index = [];
    this.dateBlock.setHours(this.dateBlock.getHours() + 24);
    this.httpClient.get(this.url + '/sites/')
      .subscribe((response: { sites: Site[] }) => {
        if (response.sites.length > 0) {
          this.sites = response.sites;
        } else {
          console.log('NO DATA FOUND');
        }
      });
    this.httpClient.get(this.url + '/order/user/'  + this.user.id)
      .subscribe((response: { orders: Orders[]}) => {
        if (response.orders.length > 0 ) {
          this.orders = response.orders;
          for (let i = 0; i < this.orders.length; i++) {
            this.index.push(i);
          }
          for (const order of this.orders) {
            this.getSite(order.idSite);
          }
        }
      });
  }

  createOrder(nbTrays) {
    if (this.siteSelected && this.dateSelected) {
      this.httpClient.post(this.url + '/order', {
        nbTrays,
        date: this.date,
        idUser: this.user.id,
        idSite: this.idSite
      }).subscribe((response: {success: boolean}) => {
        if (response.success) {
          this.toastr.success('Commande effectuée', 'Succès');
          this.ngOnInit();
        } else {
          this.toastr.error('Impossible de passer la commande', 'Erreur');
        }
      });
    } else {
      this.toastr.error('Le lieu et la date doivent être renseigner', 'Erreur');
    }
  }

  onSelect(idSite) {
    this.idSite = idSite;
    this.siteSelected = true;
  }

  onChangeDate(date) {
    this.date = date;
    this.dateSelected = true;
  }

  getSite(idSite) {
    this.httpClient.get(this.url + '/sites/' + idSite)
      .subscribe((response: { site: Site[]}) => {
        this.orderSite.push(response.site[0]);
      });
  }
}
