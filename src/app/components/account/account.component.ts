import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Md5} from 'ts-md5';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {
  private user: User;
  private userDeleted = false;
  private isDeleted = false;

  constructor(private userService: UserService, private toastr: ToastrService, private router: Router) {
    this.userService.currentUser.subscribe(user => {
      this.user = user[0];
    });
  }

  ngOnInit() {
  }

  onSubmitInfo(lastname: string, firstname: string, email: string) {
    this.userService.updateInfoUser(lastname, firstname, email, this.user.password, this.user.id);
  }

  onSubmitPassword(oldPassword: string, newPassword: string, repeatNewPassword: string) {
    const hashedPassword = Md5.hashStr(oldPassword);
    if (hashedPassword === this.user.password) {
      if (newPassword.length >= 8 && newPassword.length <= 50 ) {
        if (newPassword === repeatNewPassword) {
          this.userService.updatePassword(this.user.id, newPassword);
        } else {
          this.toastr.error('Les nouveaux passwords ne sont pas égaux', 'Erreur');
        }
      } else {
        this.toastr.error('Le nouveau mot de passe ne respècte pas les règles de longueur', 'Erreur');
      }
    } else {
      this.toastr.error('Votre ancien ne correspond pas à l\'existant', 'Erreur');
    }
  }

  onRemove() {
    this.userService.removeUser(this.user.id);
  }

  deleteUser() {
    this.userDeleted = true;
  }

  checkDeleted(value){
    if (value === 'EFFACER') {
      this.isDeleted = true;
    } else {
      this.isDeleted = false;
    }
  }
}
