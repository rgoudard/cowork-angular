import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../models/User';
import {Site} from '../../models/Site';
import {SiteHours} from '../../models/SiteHours';
import {Material} from '../../models/Material';
import {MaterialCounter} from '../../models/MaterialCounter';

@Component({
  selector: 'app-oursites',
  templateUrl: './oursites.component.html'
})
export class OursitesComponent implements OnInit {

  private user: User;
  private url = 'http://51.91.10.171:3000';
  private sites: Site[];
  private siteHours: SiteHours[];
  private materialsCounter: Array<MaterialCounter> = [];

  constructor(private userService: UserService, private router: Router, private httpClient: HttpClient,
              private toastr: ToastrService) {
    this.userService.currentUser.subscribe(user => this.user = user[0]);
  }

  ngOnInit() {
    this.httpClient.get(this.url + '/sites/')
      .subscribe((response: { sites: Site[] }) => {
        if (response.sites.length > 0) {
          this.sites = response.sites;

          for (let i = 0 ; i < this.sites.length; i++) {
            this.httpClient.get(this.url + '/material/site/' + this.sites[i].id)
              .subscribe((responses: {material: Material[]}) => {
                let countSR = 0;
                let countSA = 0;
                let countSalon = 0;
                let countPrinter = 0;
                let countComputer = 0;
                for (let y = 0; y < responses.material.length; y++) {
                  if (responses.material[y].type === 'SR') { countSR += 1; }
                  if (responses.material[y].type === 'SA') { countSA += 1; }
                  if (responses.material[y].type === 'Salon') { countSalon += 1; }
                  if (responses.material[y].type === 'Imprimante') { countPrinter += 1; }
                  if (responses.material[y].type === 'Ordinateur') { countComputer += 1; }
                }
                this.materialsCounter.push({idSite: this.sites[i].id, countSR, countSA, countSalon, countPrinter, countComputer});
              });
          }
        } else {
          console.log('NO DATA FOUND');
        }
      });

    this.httpClient.get(this.url + '/siteHours')
      .subscribe((response: { siteHours: SiteHours[] }) => {
        if (response.siteHours.length > 0) {
          this.siteHours = response.siteHours;
        }
      });
  }
}
